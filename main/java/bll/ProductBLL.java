package bll;

import bll.validators.ProductIDValidator;
import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Product bll.
 */
public class ProductBLL {
    private List<Validator<Product>> validators = new ArrayList<>();
    private ProductDAO productDAO = new ProductDAO();

    /**
     * Instantiates a new Product bll.
     */
    public ProductBLL (){
        validators.add(new QuantityValidator());
    }
    private void validateproduct(Product p){
        for (Validator<Product> arr : validators){
            arr.validate(p);
        }
    }

    /**
     * Add product.
     *
     * @param p the p
     * @throws IntrospectionException the introspection exception
     * @throws IllegalAccessException the illegal access exception
     */
    public void addProduct(Product p) throws IntrospectionException, IllegalAccessException {
        validateproduct(p);
        productDAO.insert(p);
    }

    /**
     * Remove product.
     *
     * @param c the c
     * @throws IllegalAccessException    the illegal access exception
     * @throws IntrospectionException    the introspection exception
     * @throws InvocationTargetException the invocation target exception
     */
    public void removeProduct(Product c) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        validateproduct(c);
        productDAO.delete(c);
    }

    /**
     * Update product.
     *
     * @param c the c
     * @throws IllegalAccessException    the illegal access exception
     * @throws IntrospectionException    the introspection exception
     * @throws InvocationTargetException the invocation target exception
     */
    public void updateProduct(Product c) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        validateproduct(c);
        productDAO.update(c);
    }

    /**
     * Show all products object [ ] [ ].
     *
     * @return the object [ ] [ ]
     * @throws IllegalAccessException the illegal access exception
     */
    public Object[][] showAllProducts() throws IllegalAccessException {
        Object [][]mat = (Object[][]) productDAO.createTableData();
        return mat;
    }

    /**
     * Get product id product.
     *
     * @param id the id
     * @return the product
     */
    public Product getProductID(int id){
        Product c = productDAO.findById(id);
        return  c;
    }
}
