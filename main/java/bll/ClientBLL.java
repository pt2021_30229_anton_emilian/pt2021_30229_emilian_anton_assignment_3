package bll;

import bll.validators.EmailValidator;
import bll.validators.PhoneValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Client bll.
 */
public class ClientBLL {
    private List<Validator<Client>> validators = new ArrayList<>();
    private ClientDAO clientDAO= new ClientDAO();

    /**
     * Instantiates a new Client bll.
     */
    public ClientBLL(){
        validators.add(new EmailValidator());
        validators.add(new PhoneValidator());
    }

    private void validateClient(Client c){
        for (Validator<Client> arr : validators){
            arr.validate(c);
        }
    }

    /**
     * Add client.
     *
     * @param c the c
     * @throws IntrospectionException the introspection exception
     * @throws IllegalAccessException the illegal access exception
     */
    public void addClient(Client c) throws IntrospectionException, IllegalAccessException {
            validateClient(c);
            clientDAO.insert(c);
    }

    /**
     * Remove client.
     *
     * @param c the c
     * @throws IllegalAccessException    the illegal access exception
     * @throws IntrospectionException    the introspection exception
     * @throws InvocationTargetException the invocation target exception
     */
    public void removeClient(Client c) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        validateClient(c);
        clientDAO.delete(c);
    }

    /**
     * Update client.
     *
     * @param c the c
     * @throws IllegalAccessException    the illegal access exception
     * @throws IntrospectionException    the introspection exception
     * @throws InvocationTargetException the invocation target exception
     */
    public void updateClient(Client c) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        validateClient(c);
        clientDAO.update(c);
    }

    /**
     * Get client id client.
     *
     * @param id the id
     * @return the client
     */
    public Client getClientID(int id){
        Client c = clientDAO.findById(id);
        return  c;
    }

    /**
     * Show all clients object [ ] [ ].
     *
     * @return the object [ ] [ ]
     * @throws IllegalAccessException the illegal access exception
     */
    public Object[][] showAllClients() throws IllegalAccessException {
        Object [][]mat = (Object[][]) clientDAO.createTableData();
        return mat;
    }

    /**
     * Find client by id client.
     *
     * @param id the id
     * @return the client
     */
    public Client findClientByID( int id){
        Client c = clientDAO.findById(id);
        return c ;
    }

}
