package presentation;

import model.Client;
import model.Order;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

/**
 * The type Main controller.
 */
public class MainController {

    private MainView mainView ;

    /**
     * Instantiates a new Main controller.
     */
    public MainController(){
        mainView = new MainView();
        mainView.addClientButtonListener(new ClientListener());
        mainView.addProductButtonListener(new ProductListener());
        mainView.addOrderButtonListener(new OrderListener());
    }

    /**
     * The type Client listener.
     */
    class ClientListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ClientView clientView = new ClientView();
            new ClientController(clientView);
            mainView.setVisible(false);
        }
    }

    /**
     * The type Product listener.
     */
    class ProductListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ProductView productView = new ProductView();
            new ProductController(productView);
            mainView.setVisible(false);
        }
    }

    /**
     * The type Order listener.
     */
    class OrderListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            OrderView orderView = new OrderView();
            new OrderController(orderView);
        }
    }
}
