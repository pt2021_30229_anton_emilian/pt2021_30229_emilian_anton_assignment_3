package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The type Product view.
 */
public class ProductView extends  JFrame {
    private JTextField idJT = new JTextField();
    private JTextField nameJT = new JTextField();
    private JTextField priceJT = new JTextField();
    private JTextField quantityJT = new JTextField();


    private JButton addProductButton = new JButton("Add a new Product") ;
    private JButton editProductButton = new JButton("Edit Product") ;
    private JButton deleteProductButton = new JButton("Delete Product") ;
    private JButton viewAllProductsButton = new JButton("Display Product Table") ;
    private JButton backButton = new JButton("Back") ;


    /**
     * Instantiates a new Product view.
     */
    public ProductView() {
        this.setTitle("Order Management Products");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(450,750);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(245, 255, 255, 111));
        idJT.setPreferredSize(new Dimension(100,30));
        nameJT.setPreferredSize(new Dimension(100,30));
        priceJT.setPreferredSize(new Dimension(100,30));
        quantityJT.setPreferredSize(new Dimension(100,30));

        JPanel panelId = new JPanel() ;
        panelId.setBounds(0 , 100-50 , 450 , 50);
        panelId.add(new JLabel("ID"));
        panelId.add(idJT);

        JPanel panelname = new JPanel() ;
        panelname.setBounds(0 , 150-50 , 450 , 50);
        panelname.add(new JLabel("Name"));
        panelname.add(nameJT);

        JPanel panelEmail = new JPanel();
        panelEmail.setBounds(0 , 200-50 , 450 , 50);
        panelEmail.add(new JLabel("Price"));
        panelEmail.add(priceJT);

        JPanel panelTelephone = new JPanel();
        panelTelephone.setBounds(0 , 250-50 , 450 , 50);
        panelTelephone.add(new JLabel("Quantity"));
        panelTelephone.add(quantityJT);

        JPanel panelOne = new JPanel() ;
        panelOne.setBounds(0 , 350-50 , 450 , 50);
        panelOne.add(addProductButton);

        JPanel panelTwo = new JPanel() ;
        panelTwo.setBounds(0 , 400-50 , 450 , 50);
        panelTwo.add(editProductButton);

        JPanel panelThree = new JPanel() ;
        panelThree.setBounds(0 , 450-50 , 450 , 50);
        panelThree.add(deleteProductButton);

        JPanel panel4= new JPanel() ;
        panel4.setBounds(0 , 500-50 , 450 , 50);
        panel4.add(viewAllProductsButton);

        JPanel panel5 = new JPanel() ;
        panel5.setBounds(0 , 650-50 , 450 , 50);
        panel5.add(backButton);

        JPanel allPanels = new JPanel();
        allPanels.add(panelname);
        allPanels.add(panelId);
        allPanels.add(panelTelephone);
        allPanels.add(panelEmail);
        allPanels.add(panelOne);
        allPanels.add(panelTwo);
        allPanels.add(panelThree);
        allPanels.add(panel4);
        allPanels.add(panel5);
        allPanels.setLayout(null);
        this.setContentPane(allPanels);

    }

    /**
     * Gets id jt.
     *
     * @return the id jt
     */
    public String getIdJT() {
        return idJT.getText();
    }

    /**
     * Gets name jt.
     *
     * @return the name jt
     */
    public String getNameJT() {
        return nameJT.getText();
    }

    /**
     * Gets price jt.
     *
     * @return the price jt
     */
    public String getPriceJT() {
        return priceJT.getText();
    }

    /**
     * Gets quantity jt.
     *
     * @return the quantity jt
     */
    public String getQuantityJT() {
        return quantityJT.getText();
    }

    /**
     * Add insert button listener.
     *
     * @param a the a
     */
    public void addInsertButtonListener(ActionListener a){ addProductButton.addActionListener( a);}

    /**
     * Add edit button listener.
     *
     * @param e the e
     */
    public void addEditButtonListener(ActionListener e){ editProductButton.addActionListener( e);}

    /**
     * Add delete button listener.
     *
     * @param d the d
     */
    public void addDeleteButtonListener(ActionListener d){ deleteProductButton.addActionListener( d);}

    /**
     * Add view button listener.
     *
     * @param s the s
     */
    public void addViewButtonListener(ActionListener s){ viewAllProductsButton.addActionListener( s);}

    /**
     * Add back button listener.
     *
     * @param b the b
     */
    public void addBackButtonListener (ActionListener b){
        backButton.addActionListener(b);
    }
}
